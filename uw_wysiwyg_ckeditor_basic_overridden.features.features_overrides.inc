<?php

/**
 * @file
 * uw_wysiwyg_ckeditor_basic_overridden.features.features_overrides.inc
 */

/**
 * Implements hook_features_override_default_overrides().
 */
function uw_wysiwyg_ckeditor_basic_overridden_features_override_default_overrides() {
  // This code is only used for UI in features. Exported alters hooks do the magic.
  $overrides = array();

  // Exported overrides for: filter.
  $overrides["filter.uw_tf_basic.filters|wysiwyg|settings|valid_elements"] = '@[class|title|id|lang],
        a[href|rel|rev|name],
        img[!src|alt|longdesc|width|height],
        table[width],
        th[abbr|axis|headers|scope|colspan|rowspan|width],
        td[abbr|axis|headers|scope|colspan|rowspan|width],
        colgroup[span|width],
        col[span|width],
        abbr/acronym, sub, sup, dfn, samp, kbd, var,
        ol[start],
        em, i, strong, b, strike, del, ins, cite, blockquote, address, code, pre, ul, li[value], dl, dt, dd, p, h2, h3, h4, h5, h6, span, div, thead, tfoot, tbody, tr, caption, hr, br';

  // Exported overrides for: wysiwyg.
  $overrides["wysiwyg.uw_tf_basic.settings|buttons|default|BulletedList"] = 1;

  return $overrides;
}

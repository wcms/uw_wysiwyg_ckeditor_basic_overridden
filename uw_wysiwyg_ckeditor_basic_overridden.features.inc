<?php

/**
 * @file
 * uw_wysiwyg_ckeditor_basic_overridden.features.inc
 */

/**
 * Implements hook_filter_default_formats_alter().
 */
function uw_wysiwyg_ckeditor_basic_overridden_filter_default_formats_alter(&$data) {
  if (isset($data['uw_tf_basic'])) {
    $data['uw_tf_basic']['filters']['wysiwyg']['settings']['valid_elements'] = '@[class|title|id|lang],
          a[href|rel|rev|name],
          img[!src|alt|longdesc|width|height],
          table[width],
          th[abbr|axis|headers|scope|colspan|rowspan|width],
          td[abbr|axis|headers|scope|colspan|rowspan|width],
          colgroup[span|width],
          col[span|width],
          abbr/acronym, sub, sup, dfn, samp, kbd, var,
          ol[start],
          em, i, strong, b, strike, del, ins, cite, blockquote, address, code, pre, ul, li[value], dl, dt, dd, p, h2, h3, h4, h5, h6, span, div, thead, tfoot, tbody, tr, caption, hr, br'; /* WAS: '@[class|title|id|lang],
    a[href|rel|rev|name],
    img[!src|alt|longdesc|width|height],
    table[width],
    th[abbr|axis|headers|scope|colspan|rowspan|width],
    td[abbr|axis|headers|scope|colspan|rowspan|width],
    colgroup[span|width],
    col[span|width],
    abbr/acronym, sub, sup, dfn, samp, kbd, var,
    ol[start],
    em, i, strong, b, strike, del, ins, cite, blockquote, address, code, pre, ul, li[value], dl, dt, dd, p, h2, h3, h4, h5, h6, span, div, thead, tfoot, tbody, tr, caption, hr, br' */
  }
}

/**
 * Implements hook_wysiwyg_default_profiles_alter().
 */
function uw_wysiwyg_ckeditor_basic_overridden_wysiwyg_default_profiles_alter(&$data) {
  if (isset($data['uw_tf_basic'])) {
    $data['uw_tf_basic']['settings']['buttons']['default']['BulletedList'] = 1; /* WAS: '' */
  }
}
